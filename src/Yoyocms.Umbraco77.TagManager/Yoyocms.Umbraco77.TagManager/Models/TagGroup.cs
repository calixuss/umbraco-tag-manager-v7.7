﻿using Umbraco.Core.Persistence;

namespace Yoyocms.Umbraco77.TagManager.Models
{
    [TableName("cmsTags")]
    public class TagGroup
    {
        public string group { get; set; }
        public int groupId { get; set; }
    }
}