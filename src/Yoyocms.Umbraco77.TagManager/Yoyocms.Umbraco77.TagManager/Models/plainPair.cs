﻿using System.Runtime.Serialization;

namespace Yoyocms.Umbraco77.TagManager.Models
{
    [DataContract(Name = "plainPair", Namespace = "")]
    public class plainPair
    {
        [DataMember(Name = "id")]
        public int id { get; set; }

        [DataMember(Name = "tag")]
        public string tag { get; set; }
    }
}