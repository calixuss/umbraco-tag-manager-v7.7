﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Core.Persistence;

namespace Yoyocms.Umbraco77.TagManager
{
    public class InstallHelper
    {
        protected const string TagManagerSectionAlias = "TagManager";

        private readonly string _adminAlias = "admin";
        private readonly string[] groupsV77 = { "admin", "editor", "writer" }; //admin, writer, editor

        public void AddUserAccess()
        {
            var logger = LoggerResolver.Current.Logger;
            var _dbContext = ApplicationContext.Current.DatabaseContext;
            var _dbHelper = new DatabaseSchemaHelper(_dbContext.Database, logger, _dbContext.SqlSyntax);


            // Umbraco 7.7+ - add access to admin, writers and editors
            if (_dbHelper.TableExist("umbracoUserGroup2App"))
            {
                foreach (string groupAlias in groupsV77)
                {
                    int? groupId = _dbContext.Database
                    .ExecuteScalar<int?>("select id from umbracoUserGroup where userGroupAlias = @0", groupAlias);
                    if (groupId.HasValue && groupId != 0)
                    {
                        int rows = _dbContext.Database
                            .ExecuteScalar<int>("select count(*) from umbracoUserGroup2App where userGroupId = @0 and app = @1",
                                groupId.Value, TagManagerSectionAlias);
                        if (rows == 0)
                        {
                            _dbContext.Database.Execute("insert umbracoUserGroup2App values (@0, @1)",
                            groupId, TagManagerSectionAlias);
                        }

                    }
                }

            }
            // Umbraco 7.2 - 7.6 - add access to all admins only, admins can add other users
            else if (_dbHelper.TableExist("UmbracoUser2app"))
            {
                string sql = @"select u.id from umbracoUser as u
                            inner join umbracoUserType as t on u.userType = t.id
                                where t.userTypeAlias = @0";
                foreach (int userId in _dbContext.Database.Query<int>(sql, _adminAlias))
                {
                    int rows = _dbContext.Database
                        .ExecuteScalar<int>("select count(*) from UmbracoUser2app where [user] = @0 and app = @1",
                            userId, TagManagerSectionAlias);
                    if (rows == 0)
                    {
                        _dbContext.Database.Execute("insert UmbracoUser2app values (@0, @1)",
                            userId, TagManagerSectionAlias);
                    }
                }

            }
        }
    }
}