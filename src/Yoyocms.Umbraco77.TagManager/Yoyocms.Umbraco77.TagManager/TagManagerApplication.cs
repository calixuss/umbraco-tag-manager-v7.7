﻿using umbraco.businesslogic;
using umbraco.interfaces;

namespace Yoyocms.Umbraco77.TagManager
{
    [Application("TagManager", "Tag Manager", "icon-tags", 15)]
    public class TagManagerApplication : IApplication
    {
    }
}