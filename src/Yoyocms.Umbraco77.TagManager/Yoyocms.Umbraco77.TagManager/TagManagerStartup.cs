﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Umbraco.Core;
using Umbraco.Core.IO;
using Umbraco.Core.Models;

namespace Yoyocms.Umbraco77.TagManager
{
    public class TagManagerStartup : ApplicationEventHandler
    {

        protected override void ApplicationStarted(UmbracoApplicationBase umbraco, ApplicationContext context)
        {
            // Grant access to the new section           
            InstallHelper installHelper = new InstallHelper();
            installHelper.AddUserAccess();
        }
    }
}
